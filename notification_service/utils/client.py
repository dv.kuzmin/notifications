from typing import Any
from urllib.parse import urljoin

import requests


class HTTPClient:
    """Base class for variable http clients"""
    def __init__(self, base_url: str, token: str = None, content_type: str = None):
        self._base_url = base_url
        self._token = token
        self._content_type = content_type
        self.headers = {"headers": {}}
        self._set_headers()

    def _set_headers(self) -> None:
        if self._token is not None:
            self.headers["headers"]["Authorization"] = self._token

        if self._content_type is not None:
            self.headers["headers"]["Content-Type"] = self._content_type

    def get(self, endpoint_path: str, **request_params) -> Any:
        url: str = urljoin(self._base_url, endpoint_path)
        response = requests.get(url, params=request_params, headers=self.headers["headers"])

        return response

    def post(self, endpoint_path: str, data: str, **request_params) -> Any:
        url: str = urljoin(self._base_url, endpoint_path)
        response = requests.post(url, data=data, params=request_params, headers=self.headers["headers"])

        return response
