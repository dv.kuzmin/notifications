import os

from celery import Celery
from celery.schedules import crontab

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "configs.settings")

app = Celery("notifications")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()


# Settings for periodic task send mail with celery
app.conf.beat_schedule = {
    "daily_mail_sending": {
        "task": "apps.main.tasks.daily_mail_statistic",
        "schedule": crontab(minute="*/5"),
    },
}
