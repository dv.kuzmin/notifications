import re

from rest_framework import serializers
import pytz

from .models import Notification, Client


class NotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        fields = '__all__'

    def validate(self, data):
        mobile_code_pattern = r"\d{3}\b"
        filters = ("mobile_code_list", "tag_list")

        if not isinstance(data["client_filters"], dict):
            raise serializers.ValidationError("'client_filters' field must be a dict object")

        if len(data["client_filters"]) > 2:
            raise serializers.ValidationError("Too much filters, you can use only 'mobile_code_tag' and 'tag_list'")

        if data["client_filters"] and len(data["client_filters"]) < 3:
            for client_filter in data["client_filters"]:
                if client_filter not in filters:
                    raise serializers.ValidationError("No such filter name, please use 'mobile_code_tag' or 'tag_list'")
                if client_filter == "mobile_code_list":
                    for mobile_code in data["client_filters"]["mobile_code_list"]:
                        if not re.match(mobile_code_pattern, str(mobile_code)):
                            raise serializers.ValidationError("'mobile_code' filter should consist of 3 digits")

        return data


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'

    def validate(self, data):
        phone_pattern = r"7\d{10}\b"
        if not re.match(phone_pattern, str(data["phone_number"])):
            raise serializers.ValidationError("Wrong phone number format. Use '7XXXXXXXXXX'")

        if data.get("time_zone") and data["time_zone"] not in pytz.all_timezones:
            raise serializers.ValidationError("Wrong time zone format")

        return data
