from django.contrib import admin

from .models import Notification, Message, Client

admin.site.register(Notification)
admin.site.register(Message)
admin.site.register(Client)
