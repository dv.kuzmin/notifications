# Generated by Django 4.1.3 on 2022-11-24 15:34

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='notification',
            old_name='client_filter',
            new_name='client_filters',
        ),
    ]
