from django.urls import path, include

from .views import (
    NotificationCreateRetrieveView,
    NotificationUpdateDestroyView,
    ClientCreateView,
    ClientUpdateDestroyView,
)

urlpatterns = [
    path('notifications/', NotificationCreateRetrieveView.as_view()),
    path('notifications/<int:pk>/', NotificationUpdateDestroyView.as_view()),
    path('clients/', ClientCreateView.as_view()),
    path('clients/<int:pk>/', ClientUpdateDestroyView.as_view()),
]

urlpatterns = [
    path('v1/', include(urlpatterns)),
]
