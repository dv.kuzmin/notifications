from apps.main.models import Notification


def collect_notifications_statistics() -> list:
    """Get full statistics for all notifications"""
    notifications = Notification.objects.prefetch_related("message_set").all()
    notifications_statistics = []
    for notification in notifications:
        notification_data = {f"notification # {notification.id}": {}}
        messages = notification.message_set.all()
        for message in messages:
            if message.status not in notification_data[f"notification # {notification.id}"]:
                notification_data[f"notification # {notification.id}"][message.status] = 1
            else:
                notification_data[f"notification # {notification.id}"][message.status] += 1
        notifications_statistics.append(notification_data)

    return notifications_statistics


def get_notification_statistics(notification: Notification) -> dict:
    """Get detail statistics for one notification"""
    messages = notification.message_set.all()
    detail_notification_statistic = {
        f"notification # {notification.pk}": {"text": f"{notification.text}", "messages in notification": {}}}

    for message in messages:
        detail_notification_statistic[f"notification # {notification.pk}"]["messages in notification"][
            f"message #{message.pk}"] = {}
        detail_notification_statistic[f"notification # {notification.pk}"]["messages in notification"][
            f"message #{message.pk}"]["status"] = message.status
        detail_notification_statistic[f"notification # {notification.pk}"]["messages in notification"][
            f"message #{message.pk}"]["client phone number"] = message.client.phone_number

    return detail_notification_statistic
