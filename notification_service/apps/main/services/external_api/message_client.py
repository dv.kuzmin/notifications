from typing import Any
from django.conf import settings
from utils.client import HTTPClient


class MessageAPIClient:
    """Http client, which works with message service"""
    def __init__(self):
        self._http_client = HTTPClient(settings.MESSAGE_API_URL,
                                       settings.MESSAGE_API_KEY)

    def send_message(self, message_id: int, data: str) -> Any:
        endpoint_path = f"send/{message_id}"
        return self._http_client.post(endpoint_path=endpoint_path, data=data)
