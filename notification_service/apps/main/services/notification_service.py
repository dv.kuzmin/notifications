import json
from datetime import datetime
import time
from typing import List
import logging

from apps.main.models import Notification, Client, Message
from apps.main.services.external_api import MessageAPIClient

logger = logging.getLogger("message_logger")


class NotificationService:
    def __init__(self, data: dict, client: MessageAPIClient):
        self.data = data
        self.notification = self.create_notification()
        self.undelivered_messages = []
        self.client = client

    def create_notification(self) -> Notification:
        datetime_format = "%Y-%m-%dT%H:%M:%SZ"
        notification = Notification.objects.create(
            start_datetime=datetime.strptime(self.data["start_datetime"], datetime_format),
            end_datetime=datetime.strptime(self.data["end_datetime"], datetime_format),
            text=self.data["text"],
            client_filters=self.data["client_filters"]
        )
        logger.info(f"Created notification with id {notification.pk}")

        return notification

    def _get_clients_by_tag(self, tag: str) -> List[Client]:
        return list(Client.objects.filter(tag=tag))

    def _get_clients_by_mobile_code(self, mobile_code: str) -> List[Client]:
        clients = Client.objects.all()
        target_clients = []

        for client in clients:
            if client.phone_number[1:4] == mobile_code:
                target_clients.append(client)

        return target_clients

    @property
    def datetime_range_is_valid(self) -> bool:
        if datetime.now() > self.notification.end_datetime:
            return False
        while True:
            if not self.notification.start_datetime <= datetime.now():
                time.sleep(3)
            else:
                return True

    def _create_message(self, client: Client) -> Message:
        return Message.objects.create(
            status="waiting",
            notification=self.notification,
            client=client
        )

    def _send_message(self, message) -> None:
        send_data = {
            "id": message.pk,
            "phone": message.client.phone_number,
            "text": self.notification.text
        }

        send_data_json = json.dumps(send_data)
        logger.info(f"sending message with id {send_data['id']}...")
        response = self.client.send_message(message_id=message.pk, data=send_data_json)
        if response.status_code != 200:
            logger.warning(f"Deliver message with id {send_data['id']} failed")
            self.undelivered_messages.append(message)
            message.status = "deliver_failed"
            message.save()
        else:
            logger.info(f"Message with id {send_data['id']} was successfully delivered!")
            message.status = "delivered"
            message.save()

    def send_messages(self, clients: List[Client]) -> None:
        if self.datetime_range_is_valid:
            for client in clients:
                message = self._create_message(client)
                self._send_message(message)

    def _get_clients_by_filters(self) -> List[Client]:
        clients = []
        if not self.data.get("client_filters"):
            clients = Client.objects.all()
            return clients

        if client_tag_list := self.data["client_filters"].get("tag_list"):
            for tag in client_tag_list:
                clients.extend(self._get_clients_by_tag(tag))

        if mobile_code_list := self.data["client_filters"].get("mobile_code_list"):
            for mobile_code in mobile_code_list:
                clients.extend(self._get_clients_by_mobile_code(str(mobile_code)))

        return clients

    def handle_notification(self) -> None:
        if self.datetime_range_is_valid:
            clients = self._get_clients_by_filters()
            self.send_messages(clients)

            while self.undelivered_messages and datetime.now() < self.notification.end_datetime:
                for message in self.undelivered_messages:
                    self._send_message(message)
