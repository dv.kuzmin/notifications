import logging

from rest_framework import generics
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework import views
from drf_spectacular.utils import extend_schema

from .models import Notification, Client
from .serializers import NotificationSerializer, ClientSerializer
from .tasks import handle_notification_task
from .services import collect_notifications_statistics, get_notification_statistics

logger = logging.getLogger("message_logger")


class NotificationCreateRetrieveView(views.APIView):

    @extend_schema(
        request=NotificationSerializer,
        responses={200: NotificationSerializer}
    )
    def get(self, request: Request):
        notification_statistics = collect_notifications_statistics()
        logger.info("request for notification statistics")
        return Response(data=notification_statistics)

    @extend_schema(
        request=NotificationSerializer,
        responses={201: NotificationSerializer}
    )
    def post(self, request: Request):
        serializer = NotificationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        logger.info(f"request for creating notification")

        handle_notification_task.delay(serializer.validated_data)

        return Response(data={"message": "Your notification is handling"})


class NotificationUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Notification.objects.all()
    serializer_class = NotificationSerializer

    @extend_schema(
        request=NotificationSerializer,
        responses={200: NotificationSerializer}
    )
    def get(self, request, *args, **kwargs):
        notification = self.get_object()
        detail_notification_statistic = get_notification_statistics(notification)

        return Response(detail_notification_statistic)


class ClientCreateView(generics.CreateAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class ClientUpdateDestroyView(generics.UpdateAPIView, generics.DestroyAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
