import json

from configs.celery import app
from django.core.mail import send_mail

from .services import NotificationService, collect_notifications_statistics
from apps.main.services.external_api import MessageAPIClient


@app.task
def handle_notification_task(data: dict):
    """Handle new notification checking for correct datetime range and success status of delivered message"""
    notification_service = NotificationService(data=data, client=MessageAPIClient())
    notification_service.handle_notification()


@app.task
def daily_mail_statistic():
    """Periodic task for daily mail sending statistics"""
    notification_statistics = collect_notifications_statistics()
    notification_statistics_json = json.dumps(notification_statistics)
    send_mail(
        "Daily Notification",
        f"{notification_statistics_json}",
        "kuzmindv87@gmail.com",
        ["dk_187@mail.ru"],
        fail_silently=False
    )
