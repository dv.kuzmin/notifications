from django.db import models


class Client(models.Model):
    phone_number = models.CharField(max_length=11, verbose_name="Номер телефона")
    tag = models.CharField(max_length=255, verbose_name="Метка")
    time_zone = models.CharField(max_length=255, default="Europe/Moscow", verbose_name="Часовой пояс")

    class Meta:
        verbose_name = "Клиент"
        verbose_name_plural = "Клиенты"

    def __str__(self):
        return f"Клиент с номером телефона {self.phone_number}"


class Notification(models.Model):
    start_datetime = models.DateTimeField(verbose_name="Время начала рассылки")
    end_datetime = models.DateTimeField(verbose_name="Время окончания рассылки")
    text = models.TextField(verbose_name="Текст сообщения")
    client_filters = models.JSONField(verbose_name="Фильтр рассылки")

    class Meta:
        verbose_name = "Рассылка"
        verbose_name_plural = "Рассылки"

    def __str__(self):
        return self.text


MESSAGE_STATUSES = (
    ("delivered", "Сообщение доставлено"),
    ("deliver failed", "Сообщение не доставлено"),
    ("waiting", "Сообщение ожидает отправки"),
)


class Message(models.Model):
    create_datetime = models.DateTimeField(auto_now=True, verbose_name="Время отправки сообщения")
    status = models.CharField(max_length=100, choices=MESSAGE_STATUSES, verbose_name="Статус")
    notification = models.ForeignKey(Notification, on_delete=models.CASCADE, verbose_name="Рассылка")
    client = models.ForeignKey(Client, on_delete=models.CASCADE, verbose_name="Клиент")

    class Meta:
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"

    def __str__(self):
        return f"Сообщение для клиента {self.client} в рамках рассылки {self.notification}"
